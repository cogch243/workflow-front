const webpack = require("webpack");

export default {
  components: true,
  modules: [
    "bootstrap-vue/nuxt",
    [
      "nuxt-fontawesome",
      {
        component: "fa",
        imports: [
          {
            set: "@fortawesome/free-solid-svg-icons",
            icons: ["fas"],
          },
          {
            set: "@fortawesome/free-brands-svg-icons",
            icons: ["fab"],
          },
        ],
      },
    ],
  ],
  router: {
    middleware: "i18n",
    linkActiveClass: "active",
  },
  plugins: [
    "~/plugins/i18n.js",
    "~/plugins/fontawesome.js",
    "~/plugins/bootstrap.js",
    "~/plugins/bootstrap-table.js",
    "~/plugins/mxgraph.client.js",
    { src: "~/plugins/ol.js", ssr: false },
    { src: "~/plugins/vue-chartjs.js", ssr: false },
    {
      src: "~/plugins/jqwidgets-scripts",
      ssr: false,
      mode: "client",
    },
    {
      src: "~/plugins/vue2-daterange-picker",
      ssr: false,
      mode: "client",
    },
  ],
  css: ["~node_modules/bootstrap/dist/css/bootstrap.min.css", "jqwidgets-scripts/jqwidgets/styles/jqx.base.css", "~/assets/stylesheet.css", "vue2-daterange-picker/dist/vue2-daterange-picker.css"],
  build: {
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "window.jQuery": "jquery",
      }),
    ],

    transpile: [/^jqwidgets-scripts/],
    vendor: ["axios", "jqwidgets-scripts"],
    extend(config) {
      const vueLoader = config.module.rules.find((rule) => rule.loader === "vue-loader");
      vueLoader.options.transformAssetUrls = {
        video: ["src", "poster"],
        source: "src",
        img: "src",
        image: "xlink:href",
        "b-avatar": "src",
        "b-img": "src",
        "b-img-lazy": ["src", "blank-src"],
        "b-card": "img-src",
        "b-card-img": "src",
        "b-card-img-lazy": ["src", "blank-src"],
        "b-carousel-slide": "img-src",
        "b-embed": "src",
      };
    },
  },
};
