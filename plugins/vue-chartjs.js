import Vue from "vue";
import { Line, Pie } from "vue-chartjs";

Vue.component("vc-line-chart", {
  extends: Line,
  props: ["data", "options"],
  mounted() {
    this.renderChart(this.data, this.options);
  },
});

Vue.component("vc-pie-chart", {
  extends: Pie,
  props: ["data", "options"],
  mounted() {
    this.renderChart(this.data, this.options);
  },
});
