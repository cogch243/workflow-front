import Vue from 'vue'
import JqwidgetsScripts from 'jqwidgets-scripts'

jqx.credits = '75CE8878-FCD1-4EC7-9249-BA0F153A5DE8'

export default () => {
  if (process.client) {
    Vue.use(JqwidgetsScripts)
  }
}
