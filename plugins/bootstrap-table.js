import Vue from 'vue'
import VueBootstrap4Table from 'vue-bootstrap4-table'

Vue.component('VueBootstrap4Table', VueBootstrap4Table)