import Vue from "vue";
import MxGraph from "mxgraph";


Vue.use(MxGraph);
const graphConfig = {
  mxBasePath: "/mx/", //Specifies the path in mxClient.basePath.
  mxImageBasePath: "/mx/images", // Specifies the path in mxClient.imageBasePath.
  mxLanguage: "en", // Specifies the language for resources in mxClient.language.
  mxDefaultLanguage: "en", // Specifies the default language in mxClient.defaultLanguage.
  mxLoadResources: false, // Specifies if any resources should be loaded.  Default is true.
  mxLoadStylesheets: false // Specifies if any stylesheets should be loaded.  Default is true
};

const {
  mxClient,
  mxUtils,
  mxEvent,
  mxEditor,
  mxRectangle,
  mxGraph,
  mxGeometry,
  mxCell,
  mxImage,
  mxDivResizer,
  mxObjectCodec,
  mxCodecRegistry,
  mxConnectionHandler,
  mxToolbar,
  mxGraphModel,
  mxCylinder,
  mxCellRenderer,
  mxUndoManager, // Undo/Redo
  mxConstants, // graph style
  mxCodec,
} = MxGraph(graphConfig);

window.mxClient = mxClient;
window.mxUtils = mxUtils;
window.mxRectangle = mxRectangle;
window.mxGraph = mxGraph;
window.mxEvent = mxEvent;
window.mxGeometry = mxGeometry;
window.mxCell = mxCell;
window.mxImage = mxImage;
window.mxEditor = mxEditor;
window.mxDivResizer = mxDivResizer;
window.mxObjectCodec = mxObjectCodec;
window.mxCodecRegistry = mxCodecRegistry;
window.mxConnectionHandler = mxConnectionHandler;
window.mxToolbar = mxToolbar;
window.mxGraphModel = mxGraphModel;
window.mxCellRenderer = mxCellRenderer;
window.mxCylinder = mxCylinder;
window.mxUndoManager = mxUndoManager;
window.mxConstants = mxConstants;
window.mxCodec = mxCodec;