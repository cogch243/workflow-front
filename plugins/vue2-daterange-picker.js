import Vue from "vue";
import DateRangePicker from "vue2-daterange-picker";

export default () => {
  if (process.client) {
    Vue.use(DateRangePicker);
    console.log(DateRangePicker);
  }
};
